s = 'tyUkS2zAr'

#check if there is any alphanumeric in it:
out = False
for i in range(len(s)):
  if s[i].isalnum():
    out = True
    break
print(out)

#check if there is any letter in it:
out = False
for i in range(len(s)):
  if s[i].isalpha():
    out = True
    break
print(out)

#check if there is any alphanumeric in it:
out = False
for i in range(len(s)):
  if s[i].isdigit():
    out = True
    break
print(out)

#check if there is any lower case in it:
out = False
for i in range(len(s)):
  if s[i].islower():
    out = True
    break
print(out)

#check if there is any upper case in it:
out = False
for i in range(len(s)):
  if s[i].isupper():
    out = True
    break
print(out)
